# healthcheck

# Домашнее задание

Обернуть приложение в docker-образ и запушить его на Dockerhub

# Цель:

обернёт приложение в docker-образ и запушит его на Dockerhub.

# Описание/Пошаговая инструкция выполнения домашнего задания:

Шаг 1. Создать минимальный сервис, который

1. отвечает на порту `8000`
2. имеет http-метод:\
   `GET /health/`\
   `RESPONSE: {"status": "OK"}`\
3. Шаг 2. Cобрать локально образ приложения в докер контенер под архитектуру `AMD64`.
4. Запушить образ в dockerhub На выходе необходимо предоставить

* имя репозитория и тэг на Dockerhub
* ссылку на github c Dockerfile, либо приложить Dockerfile в ДЗ

Обратите внимание, что при сборке на m1 при запуске вашего контейнера на стандартных платформах будет ошибка такого
вида:
standard_init_linux.go:228: exec user process caued: exec format error Для сборки рекомендую указать тип платформы
linux/amd64:

docker build --platform linux/amd64 -t tag Более подробно можно прочитать в
статье: https://programmerah.com/how-to-solve-docker-run-error-standard_init_linux-go219-exec-user-process-caused-exec-format-error-39221/

# Dockerfile

#### ex. 1
```text
FROM openjdk:17-alpine

ADD . /src
WORKDIR /src
RUN ./mvnw package -DskipTests
EXPOSE 8080
ENTRYPOINT ["java","-jar","target/healthcheck-0.0.1-SNAPSHOT.jar"]
```
#### ex. 2
```text
#build .jar
FROM maven:3.9.4-amazoncorretto-17
WORKDIR /app
ADD ./pom.xml /app
RUN mvn dependency:resolve

ADD ./src/ /app/src
RUN mvn install
#until this we can exec: docker run -it -p 8080:8000 healthcheck java -jar target/healthcheck-0.0.1-SNAPSHOT.jar

#using .jar
FROM amazoncorretto:17-alpine-jdk

#install additional utils
RUN curl -O https://dl.google.com/linux/direct/google-chrome-stable_current_x86_64.rpm && \
yum install google-chrome-stable_current_x86_64.rpm -y && \
yum install unzip -y && \
mkdir /opt/chrome && \
curl -O https://chromedriver.storage.googleapis.com/2.41/chromedriver_linux64.zip && \
unzip chromedriver_linux64.zip -d /opt/chrome

WORKDIR /app

CMD ["ls"]

#COPY --from=0 override files inside a container
COPY --from=0 /app/target/ /app

EXPOSE 8000

CMD ["java","-jar","healthcheck-0.0.1-SNAPSHOT.jar"]
```

# Start

1. docker build -t healthcheck .
2. docker run -it -p 8080:8000 healthcheck java -jar target/healthcheck-0.0.1-SNAPSHOT.jar
3. docker run -it -p 8080:8000 healthcheck
4. docker exec -it 1aa /bin/bash
5. docker tag healthcheck:latest coicoin/healthcheck:homework1
6. docker push coicoin/healthcheck:homework1

# Sources:

1. https://habr.com/ru/articles/487922/
2. https://habr.com/ru/companies/ruvds/articles/485650/
3. https://spring.io/guides/topicals/spring-boot-docker/

