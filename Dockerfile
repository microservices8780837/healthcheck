#build .jar
FROM maven:3.9.4-amazoncorretto-17
WORKDIR /app
ADD ./pom.xml /app
RUN mvn dependency:resolve

ADD ./src/ /app/src
RUN mvn install
#until this we can exec: docker run -it -p 8080:8000 healthcheck java -jar target/healthcheck-0.0.1-SNAPSHOT.jar

#using .jar
FROM amazoncorretto:17-alpine-jdk
WORKDIR /app

#COPY --from=0 override files inside a container
COPY --from=0 /app/target/ /app
EXPOSE 8000
CMD ["java","-jar","healthcheck-0.0.1-SNAPSHOT.jar"]